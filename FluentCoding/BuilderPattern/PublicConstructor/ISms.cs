﻿namespace FluentCoding.BuilderPattern.PublicConstructor
{
	public interface ISms
	{
		string Content { get; }

		string[] Destinations { get; }

		void Send();
	}
}
