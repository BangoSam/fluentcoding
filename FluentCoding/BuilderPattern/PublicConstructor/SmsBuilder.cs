﻿using System;
using System.Collections.Generic;

namespace FluentCoding.BuilderPattern.PublicConstructor
{
	public class SmsBuilder<TSms> where TSms : class, ISms, new()
	{
		private string content = string.Empty;

		private readonly List<string> destinations = new List<string>();

		public SmsBuilder<TSms> WithContent(string content)
		{
			this.content = string.Concat(this.content, content);

			return this;
		}

		public SmsBuilder<TSms> WithEncoding()
		{
			this.content = Uri.EscapeDataString(this.content);

			return this;
		}

		public SmsBuilder<TSms> WithDestinations(params string[] destinations)
		{
			this.destinations.AddRange(destinations);

			return this;
		}

		public TSms Build()
		{
			var sms = (TSms)Activator.CreateInstance(
				typeof(TSms), this.content, this.destinations.ToArray());

			return sms;
		}
	}
}
