﻿using System;

namespace FluentCoding.BuilderPattern.PublicConstructor
{
	public static class Demo
	{
		public static void Run()
		{
			Console.WriteLine("=============== BUILDER PATTERN WITH PUBLIC CONSTRUCTOR");
			Console.WriteLine();
			Console.WriteLine("========== IMMUTABLE SMS");
			Console.WriteLine();

			FirstImmutableSmsExample();

			Console.WriteLine();

			SecondImmutableSmsExample();

			Console.WriteLine();

			ThirdImmutableSmsExample();

			Console.WriteLine();
			Console.WriteLine("========== MUTABLE SMS");
			Console.WriteLine();

			FirstMutableSmsExample();

			Console.WriteLine();

			SecondMutableSmsExample();

			Console.WriteLine();

			ThirdMutableSmsExample();
		}

		private static void FirstImmutableSmsExample()
		{
			Console.WriteLine("===== FIRST EXAMPLE");

			var sms = ImmutableSms.New()
				.WithContent("hello world")
				.WithContent(", I am alive")
				.WithEncoding()
				.WithDestinations("Sam", "Iain")
				.Build();

			// The properties are immutable and cannot be changed!
			//sms.Content = "testing";
			//sms.Destinations = new [] { "Jack", "Jill" };

			sms.Send();
		}

		private static void SecondImmutableSmsExample()
		{
			Console.WriteLine("===== SECOND EXAMPLE");

			var sms = new ImmutableSms(
				Uri.EscapeDataString("hello world, I am alive"),
				new [] { "Sam", "Iain" });

			sms.Send();
		}

		private static void ThirdImmutableSmsExample()
		{
			Console.WriteLine("===== THIRD EXAMPLE");

			// You cannot use an object initializer!
			//var thirdSms = new ImmutableSms
			//{
				//Content = Uri.EscapeDataString("hello world, I am alive"),
				//Destinations = new [] { "Sam", "Iain" }
			//};

			Console.WriteLine("Cannot be done!");
		}

		private static void FirstMutableSmsExample()
		{
			Console.WriteLine("===== FIRST EXAMPLE");

			var sms = MutableSms.New()
				.WithContent("hello world")
				.WithContent(", I am alive")
				.WithEncoding()
				.WithDestinations("Sam", "Iain")
				.Build();

			sms.Content = "testing";
			sms.Destinations = new [] { "Jack", "Jill" };

			sms.Send();
		}

		private static void SecondMutableSmsExample()
		{
			Console.WriteLine("===== SECOND EXAMPLE");

			var sms = new MutableSms(
				Uri.EscapeDataString("hello world, I am alive"),
				new [] { "Sam", "Iain" });

			sms.Send();
		}

		private static void ThirdMutableSmsExample()
		{
			Console.WriteLine("===== THIRD EXAMPLE");

			var sms = new MutableSms
			{
				Content = Uri.EscapeDataString("hello world, I am alive"),
				Destinations = new [] { "Sam", "Iain" }
			};

			sms.Send();
		}
	}
}
