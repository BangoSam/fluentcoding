﻿using System;
using System.Linq;

namespace FluentCoding.BuilderPattern.PublicConstructor
{
	public class ImmutableSms : ISms
	{
		public string Content { get; }

		public string[] Destinations { get; }

		public ImmutableSms() { }

		public ImmutableSms(string content = null, string[] destinations = null)
		{
			this.Content = content ?? string.Empty;
			this.Destinations = destinations ?? new string[] { };
		}

		public void Send()
		{
			this.Destinations.ToList().ForEach(destination =>
				Console.WriteLine($"Sent \"{this.Content}\" to \"{destination}\"!"));
		}

		public static SmsBuilder<ImmutableSms> New()
		{
			return new SmsBuilder<ImmutableSms>();
		}
	}
}
