﻿using System;
using System.Linq;

namespace FluentCoding.BuilderPattern.PublicConstructor
{
	public class MutableSms : ISms
	{
		public string Content { get; set; }

		public string[] Destinations { get; set; }

		public MutableSms() { }

		public MutableSms(string content = null, string[] destinations = null)
		{
			this.Content = content ?? string.Empty;
			this.Destinations = destinations ?? new string[] { };
		}

		public void Send()
		{
			this.Destinations.ToList().ForEach(destination =>
				Console.WriteLine($"Sent \"{this.Content}\" to \"{destination}\"!"));
		}

		public static SmsBuilder<MutableSms> New()
		{
			return new SmsBuilder<MutableSms>();
		}
	}
}
