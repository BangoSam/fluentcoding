﻿using System;
using System.Linq;

namespace FluentCoding.BuilderPattern.PrivateConstructor
{
	public class ImmutableSms : ISms
	{
		private readonly Func<string, string[], ImmutableSms> build;

		private ImmutableSms()
		{
			this.build = (content, destinations) =>
			{
				this.Content = content;
				this.Destinations = destinations;
				return this;
			};
		}

		public string Content { get; private set; }

		public string[] Destinations { get; private set; }

		public void Send()
		{
			this.Destinations.ToList().ForEach(destination =>
				Console.WriteLine($"Sent \"{this.Content}\" to \"{destination}\"!"));
		}

		public static SmsBuilder<ImmutableSms> New()
		{
			var sms = new ImmutableSms();

			return new SmsBuilder<ImmutableSms>(sms.build);
		}
	}
}
