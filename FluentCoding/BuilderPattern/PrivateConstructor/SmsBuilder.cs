﻿using System;
using System.Collections.Generic;

namespace FluentCoding.BuilderPattern.PrivateConstructor
{
	public class SmsBuilder<TSms> where TSms : ISms
	{
		private readonly Func<string, string[], TSms> buildSms;

		private string content;

		private readonly List<string> destinations;

		public SmsBuilder(Func<string, string[], TSms> buildSms)
		{
			this.buildSms = buildSms;
			this.content = string.Empty;
			this.destinations = new List<string>();
		}

		public SmsBuilder<TSms> WithContent(string content)
		{
			this.content = string.Concat(this.content, content);

			return this;
		}

		public SmsBuilder<TSms> WithEncoding()
		{
			this.content = Uri.EscapeDataString(this.content);

			return this;
		}

		public SmsBuilder<TSms> WithDestinations(params string[] destinations)
		{
			this.destinations.AddRange(destinations);

			return this;
		}

		public TSms Build()
		{
			return this.buildSms(
				this.content,
				this.destinations.ToArray());
		}
	}
}
