﻿using System;
using System.Linq;

namespace FluentCoding.BuilderPattern.PrivateConstructor
{
	public class MutableSms : ISms
	{
		private readonly Func<string, string[], MutableSms> build;

		private MutableSms()
		{
			this.build = (content, destinations) =>
			{
				this.Content = content;
				this.Destinations = destinations;
				return this;
			};
		}

		public string Content { get; set; }

		public string[] Destinations { get; set; }

		public void Send()
		{
			this.Destinations.ToList().ForEach(destination =>
				Console.WriteLine($"Sent \"{this.Content}\" to \"{destination}\"!"));
		}

		public static SmsBuilder<MutableSms> New()
		{
			var sms = new MutableSms();

			return new SmsBuilder<MutableSms>(sms.build);
		}
	}
}
