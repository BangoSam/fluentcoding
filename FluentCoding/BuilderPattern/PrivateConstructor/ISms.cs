﻿namespace FluentCoding.BuilderPattern.PrivateConstructor
{
	public interface ISms
	{
		string Content { get; }

		string[] Destinations { get; }

		void Send();
	}
}
