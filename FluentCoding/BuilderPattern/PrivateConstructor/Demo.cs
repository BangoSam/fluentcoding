﻿using System;

namespace FluentCoding.BuilderPattern.PrivateConstructor
{
	public static class Demo
	{
		public static void Run()
		{
			Console.WriteLine("=============== BUILDER PATTERN WITH PRIVATE CONSTRUCTOR");
			Console.WriteLine();
			Console.WriteLine("========== IMMUTABLE SMS");
			Console.WriteLine();

			ImmutableSmsExample();

			Console.WriteLine();
			Console.WriteLine("========== MUTABLE SMS");
			Console.WriteLine();

			MutableSmsExample();
		}

		private static void ImmutableSmsExample()
		{
			Console.WriteLine("===== EXAMPLE");

			// You cannot instantiate it the normal way!
			//var sms = new ImmutableSms();

			var sms = ImmutableSms.New()
				.WithContent("hello world")
				.WithContent(", I am alive")
				.WithEncoding()
				.WithDestinations("Sam", "Iain")
				.Build();

			// The properties are immutable and cannot be changed!
			//sms.Content = "testing";
			//sms.Destinations = new [] { "Jack", "Jill" };

			sms.Send();
		}

		private static void MutableSmsExample()
		{
			Console.WriteLine("===== EXAMPLE");

			// You cannot instantiate it the normal way!
			//var sms = new MutableSms();

			var sms = MutableSms.New()
				.WithContent("hello world")
				.WithContent(", I am alive")
				.WithEncoding()
				.WithDestinations("Sam", "Iain")
				.Build();

			sms.Content = "testing";
			sms.Destinations = new [] { "Jack", "Jill" };

			sms.Send();
		}
	}
}
