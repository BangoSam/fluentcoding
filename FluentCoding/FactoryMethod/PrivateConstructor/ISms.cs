﻿namespace FluentCoding.FactoryMethod.PrivateConstructor
{
	public interface ISms
	{
		string Content { get; }

		string[] Destinations { get; }

		void Send();
	}
}
