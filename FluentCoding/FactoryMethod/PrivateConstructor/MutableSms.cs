﻿using System;
using System.Collections.Generic;

namespace FluentCoding.FactoryMethod.PrivateConstructor
{
	public class MutableSms : ISms
	{
		private List<string> destinations = new List<string>();

		private MutableSms()
		{
		}

		public string Content { get; set; }

		public string[] Destinations
		{
			get
			{
				return destinations.ToArray();
			}

			set
			{
				destinations.Clear();
				destinations.AddRange(value);
			}
		}

		public MutableSms WithContent(string content)
		{
			this.Content = string.Concat(this.Content, content);

			return this;
		}

		public MutableSms WithEncoding()
		{
			this.Content = Uri.EscapeDataString(this.Content);

			return this;
		}

		public MutableSms WithDestinations(params string[] destinations)
		{
			this.destinations.AddRange(destinations);

			return this;
		}

		public void Send()
		{
			this.destinations.ForEach(destination =>
				Console.WriteLine($"Sent \"{this.Content}\" to \"{destination}\"!"));
		}

		public static MutableSms New()
		{
			return new MutableSms();
		}
	}
}
