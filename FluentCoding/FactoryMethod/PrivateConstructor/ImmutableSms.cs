﻿using System;
using System.Collections.Generic;

namespace FluentCoding.FactoryMethod.PrivateConstructor
{
	public class ImmutableSms : ISms
	{
		private List<string> destinations = new List<string>();

		private ImmutableSms()
		{
		}

		public string Content { get; private set; }

		public string[] Destinations
		{
			get
			{
				return destinations.ToArray();
			}
		}

		public ImmutableSms WithContent(string content)
		{
			this.Content = string.Concat(this.Content, content);

			return this;
		}

		public ImmutableSms WithEncoding()
		{
			this.Content = Uri.EscapeDataString(this.Content);

			return this;
		}

		public ImmutableSms WithDestinations(params string[] destinations)
		{
			this.destinations.AddRange(destinations);

			return this;
		}

		public void Send()
		{
			this.destinations.ForEach(destination =>
				Console.WriteLine($"Sent \"{this.Content}\" to \"{destination}\"!"));
		}

		public static ImmutableSms New()
		{
			return new ImmutableSms();
		}
	}
}
