﻿using System;

namespace FluentCoding
{
	public class Program
	{
		public static void Main(string[] args)
		{
			Console.Title = "Fluent Coding";

			BuilderPattern.PublicConstructor.Demo.Run();

			Console.WriteLine();

			BuilderPattern.PrivateConstructor.Demo.Run();

			Console.WriteLine();

			FactoryMethod.PrivateConstructor.Demo.Run();

			Console.ReadKey();
		}
	}
}
